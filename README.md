# ANGULAR-REACT-SAMPLE #

Really simple application to demonstrate how to integrate custom ReactJS components into an existing AngularJS app.

### Project structure ###

This project has two main directories:

* __server__:
An http Express server
* __client__:
Angular-seed project customized clone to get started


### How do I get set up? ###

* clone this repo in your preferred cloud IDE or local environment `git clone git@bitbucket.org:juanjofg/angular-react-sample.git`
* in server directory, run `npm install`
* in client directory, run `bower install`

### Run the application ###

#### Local environment ###

* in server directory run `node server.js`
* open [http://localhost:3000] in your browser.

#### Cloud 9 ###

* open server/server.js file
* press 'Run' button
* in 'Preview' menu, select 'Preview Running Application'