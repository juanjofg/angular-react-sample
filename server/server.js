//BASE SETUP
//-------------------
var express     =   require('express');
var path        =   require('path');
var app = express();
var port = process.env.PORT || 3000;

app.use(express.static(path.join(__dirname, '../client/app')));

// SETUP ROUTES
var router = require('./router')(app);

// Error handling
app.use(function (err, req, res, next) {
   res.status(err.status || 500); 
});

app.listen(port);
module.exports = app;