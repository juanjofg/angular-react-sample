var express = require('express');
var router = express.Router();

// Set HOME
router.get('/', function getHome (req, res){
  res.sendfile('index.html');
});

module.exports = router;