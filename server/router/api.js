var express         = require('express');
var router          = express.Router();
var tableData       = require('./tableData.json');
var customTableData = require('./tableDatawDate.json');
// /api Default route
router.get('/', function(req, res){
  res.json({
    message: 'Angular + Plus React REST API',
    version: '1',
    date: '26/01/2016'
  });
});
// /api/tableData 
router.get('/tableData/:id', function(req, res){
  if (req.params.id == 1){
    res.json(tableData);
  } else {
    res.json(customTableData);
  }
});

module.exports = router;