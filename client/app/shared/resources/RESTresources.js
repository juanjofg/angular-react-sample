/* global angular */
(function () {
  'use strict';
  
  angular.module('myApp.REST', [])
    .factory('RESTresource', ['$http', function ($http){
      return {
        getTableData: function (id) {
          return $http.get('/api/tableData/' + id, {cache: false});
        }
      };
    }]);

})();