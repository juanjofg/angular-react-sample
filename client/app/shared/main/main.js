/* global angular */
(function () {
  'use strict';
  
  angular.module('myApp.main', [])
    .controller('MainCtrl', ['$translate', function ($translate) {
      var ctrl = this;
      ctrl.changeLang = function languageChange(langCode) {
        $translate.use(langCode);
      };
    }]);
})();