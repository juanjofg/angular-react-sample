/* global angular */
(function () {
  'use strict';
  
  angular.module('myApp.detail', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
      $routeProvider.when('/detail/:id', {
        templateUrl: 'detail/detail.html',
        controller: 'DetailCtrl',
        controllerAs: 'ctrl'
      });
    }])
    .controller('DetailCtrl', [ function(){
      var vm = this;
    }]);
})();