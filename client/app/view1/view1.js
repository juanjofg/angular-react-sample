/* global angular */
(function () {
  'use strict';
  
  angular.module('myApp.view1', ['ngRoute'])
  
    .config(['$routeProvider', function($routeProvider) {
      $routeProvider.when('/view1', {
        templateUrl: 'view1/view1.html',
        controller: 'View1Ctrl',
        controllerAs: 'ctrl',
        resolve: {
          table: ['RESTresource', function(RESTresource){
            return RESTresource.getTableData(1);
          }]
        }
      });
    }])
    
    .controller('View1Ctrl', ['table', '$location', 'RESTresource',
      function(table, $location, RESTresource) {
      
        var vm = this;
      
        vm.tableData = table.data;
        
        vm.getColumnConfig = function (index) {
          return vm.tableData.headers[index];
        };
    
        vm.gotoDetail = function (row) {
          var id = row.pop();
          $location.path('/detail/' + id);
        };
        
        vm.getMoreData = function (id) {
          RESTresource.getTableData(id).then(function (response) {
            vm.tableData = response.data;
          }, function(err){
            vm.error = err;
          });
        };
  
    }]);
  
})();