/* global angular */
(function(){
  'use strict';
  
  // Declare app level module which depends on views, and components
  angular.module('myApp', [
    'ngRoute',
    'myApp.main',
    'myApp.detail',
    'myApp.view1',
    'myApp.view2',
    'myApp.version',
    'myApp.REST',
    'pascalprecht.translate'
  ])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider.otherwise({redirectTo: '/view1'});
  }])
  .config(['$translateProvider', function ($translateProvider) {
    $translateProvider
      .useStaticFilesLoader({
        prefix: '/i18n/locale_',
        suffix: '.json'
      })
      .registerAvailableLanguageKeys(['en-us', 'es-es'], {
        'en_US': 'en-us',
        'en_UK': 'en-us',
        'es_ES': 'es-es'
      })
      .preferredLanguage('es-es')
      .useSanitizeValueStrategy(null);
    }]);

})();
